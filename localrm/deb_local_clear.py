#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2019 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

""" Script to suggest removals from apt cache based on keeping newest
only via fuzzy matching on filename
"""

# Example command 1: python2 ./deb_local_clear.py

from sys import argv,exit
from os import getenv as osgetenv
from os import path,stat
from time import gmtime
from datetime import datetime as dt
from glob import glob
import re


PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

""" Next verbosity_global = 1 is the most likely outcome
unless the user specifically overrides it by setting
the environment variable PYVERBOSITY
"""
if PYVERBOSITY is None:
	verbosity_global = 1
elif 0 == PYVERBOSITY:
	# 0 from env means 0 at global level
	verbosity_global = 0
elif PYVERBOSITY > 1:
	# >1 from env means same at global level
	verbosity_global = PYVERBOSITY
else:
	verbosity_global = 1


from string import ascii_letters,punctuation,printable
#from string import printable
SET_PRINTABLE = set(printable)
SET_PUNCT = set(punctuation)
SHELL_FIRST_LINE='#!/bin/dash'

progfull = argv[0].strip()
from os import path
progbase = path.basename(progfull)
progname = progbase.split('.')[0]
progdir = path.dirname(progbase)

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'

RM_CMD="rm -v --preserve-root"

"""
RE_WORD1=re.compile('\w+')
RE_WORD1=re.compile('[-\w]+')
RE_WORD1=re.compile('[-._\w\d]+')
RE_WORD1=re.compile('[-.\w\d]+')
"""
RE_WORD1=re.compile('[-.a-zA-Z0-9]+')
# regex above grab first [group] of word characters
# Next regex should help with detection of suffixes like deb9u2 in the filename
#RE_DEBU=re.compile('[a-z]+.*deb.*u')
RE_DEBU=re.compile('[:alphanum:]+.*deb.*u')


def tuple7fuz(str1,str2,verbosity=0):
	""" Fuzzy compare two strings and return a 7 tuple with the results
	of the comparison
	First item of the tuple (matched) is a boolean and True when fuzzy match success
	By convention str1 is the 'newer' string and str2 the older/stored string, but
	order not always important to our functionality.
	str1compare is the valid or important piece of the string we will consider for 
	fuzzy matching. Often this is shorter in length than str1

	We do not automatically .lower() the input, so if you want case insensitive then
	do your .lower() when you supply the input.
	"""
	matched = False

	str1compare = str1
	if '.deb' in str1:
		# Remove tail of file prefix and things like +deb8u1_all.deb also
		pname = str1.split('.')[0]
		if '_all' in pname:
			pos = pname.rfind('_all')
			str1compare = pname[:pos]
		else:
			str1compare = pname

	str2compare = str2
	if '.deb' in str2:
		# Remove tail of file prefix and things like +deb8u1_all.deb also
		pname2 = str2.split('.')[0]
		if '_all' in pname2:
			pos = pname2.rfind('_all')
			str2compare = pname2[:pos]
		else:
			str2compare = pname2

	if verbosity > 0 and 'udev' in str1:
		print("# {0},{1},{2},{3}".format(str1compare,str1,str2compare,str2))

	lencompare = min(len(str1compare),len(str2compare))

	if lencompare > 20:
		dist_threshold = 30
		ratio_threshold = 0.8
	elif lencompare > 15:
		dist_threshold = 25
		ratio_threshold = 0.9
	else:
		dist_threshold = 10
		ratio_threshold = 0.95

	try:
		import Levenshtein as fuz
	except ImportError:
		return (False,0,0,str1compare,str2compare,dist_threshold,ratio_threshold)

	dist = fuz.distance(str1compare,str2compare)
	ratio = fuz.ratio(str1compare,str2compare)

	if dist > 0 and ratio > 0:
		if dist < dist_threshold and ratio > ratio_threshold:
			# Fuzzy matched within defined tolerance
			matched = True
			if verbosity > 1:
				print("# {0} {1} {2} {3}".format(str1compare,dist,ratio,str2compare))

	return (matched,dist,ratio,str1compare,str2compare,dist_threshold,ratio_threshold)


def write_rm(package_dict,remove_list=[],verbosity=0,path_prefix='/var/cache/apt/archives/'):
	""" populate list labelled lines and return it
	so contains a removal/delete command
	suitable for printing to screen or piping to shell
	Setting verbosity=2 will also
	print each line as it is generated
	Making verbosity=2 will give you something that is pipeable to a shell
	"""
	lines = []
	for pname in remove_list:
		#nixtime = package_dict[pname]
		#line = "rm {0}{1}".format(path_prefix,pname)
		line = "{0} {1}{2} ;".format(RM_CMD,path_prefix,pname)
		if verbosity > 1:
			print(line)
		lines.append(line)

	if verbosity > 0:
		print("# Total of {0} removals.".format(len(lines)))

	return lines


def keep_remove_list2(daysmax=0,package_dict={},verbosity=0):
	""" Processing intended to support 'keep most recent only' retention
	policy.
	Returns two lists:
		First list is keeps (package names we suggest retaining)
		Second list is removes (package names we suggest removing)
	Unsophisticated version and intended to be bettered by a fuzzy match variant
	so has had limited testing.
	"""
	keep_list = []
	remove_list = []
	stored_pname = None
	stored_word1 = None
	for pname in sorted(package_dict, key=package_dict.get):
		word1match = RE_WORD1.match(pname)
		if word1match:
			word1=word1match.group()
			if 'gtk' in word1 and verbosity > 1:
				print("# {0}".format(word1))

			if word1 == stored_word1:
				remove_list.append(stored_pname)
				if verbosity > 1 and RE_DEBU.match(pname):
					line1 = "# list2() debNu like stored_pname: {0}".format(stored_pname)
					line2 = " superseded by {0}".format(pname)
					print("# {0}{1}".format(line1,line2))
			elif stored_pname is None:
				# Nothing to add as loop only just started
				pass
			else:
				# Add stored_pname text to keep_list
				keep_list.append(stored_pname)
			stored_word1 = word1
		stored_pname = pname

	if word1 and pname:
		""" When loop is finished we still need to postprocess
		to ensure the last entry has been added to keep_list
		"""
		if pname == keep_list[-1]:
			pass
		else:
			#print(pname,word1)
			keep_list.append(pname)

	return (keep_list,remove_list)


def keep_remove_list2fuz(daysmax=0,package_dict={},verbosity=0):
	""" Processing intended to support 'keep most recent only' retention
	policy.
	Returns two lists:
		First list is keeps (package names we suggest retaining)
		Second list is removes (package names we suggest removing)
	This variant attempts to use fuzzy matching and returns empty lists
	if unable to find a suitable fuzzy library.
	"""
	keep_list = []
	remove_list = []

	try:
		import Levenshtein as fuz
	except ImportError:
		return (keep_list,remove_list)

	stored_pname = None
	stored_word1 = None
	for pname in sorted(package_dict):
		word1match = RE_WORD1.match(pname)
		matched_flag = False
		if word1match:
			word1=word1match.group()
			if verbosity > 1:
				print("# {0},{1}".format(word1,word1))
			if 'jasper' in word1 and verbosity > 1:
				print("# {0},{1}".format(word1,pname))

			dist = 0
			ratio = 0
			if stored_pname is None:
				pass
			else:
				stored_pname_lower = stored_pname.lower()
				matched_flag,dist,ratio,str1,str2,dthresh,rthresh = tuple7fuz(pname.lower(),stored_pname_lower,verbosity)
				# dist and ratio should now be populated as distance between strings in terms of fuzzy match

			if matched_flag is True:
				# Fuzzy matched so act appropriately
				if package_dict[pname] > package_dict[stored_pname]:
					# pname has the newest timestamp on filesystem so remove older
					remove_list.append(stored_pname)
				else:
					# stored_pname has the newest timestamp on filesystem
					remove_list.append(pname)

				if verbosity > 1:
					# Any extra reporting relevant to successful Fuzzy match goes here.
					if dthresh > 10:
						print("# Fuzzy matched {0}=={1} for long string {2}".format(str1,str2,pname))
						if verbosity > 2:
							print("# Fuzzy stats for long {0} are {1}".format(str1,(dist,ratio,dthresh,rthresh)))
					elif dthresh > 1:
						print("# Fuzzy matched {0}=={1} for short string {2}".format(str1,str2,pname))
						if verbosity > 2:
							print("# Fuzzy stats for short {0} are {1}".format(str1,(dist,ratio,dthresh,rthresh)))

					else:
						pass
					if RE_DEBU.match(pname):
						line1 = "# debNu like stored_pname: {0}".format(stored_pname)
						line2 = " superseded by {0} (if newer)".format(pname)
						print("# {0}{1}".format(line1,line2))

			elif stored_pname is not None:
				keep_list.append(stored_pname)
			else:
				pass
			stored_word1 = word1
		stored_pname = pname

	if word1 and pname:
		""" When loop is finished we still need to postprocess
		to ensure the last entry has been added to keep_list
		"""
		if pname == keep_list[-1]:
			pass
		else:
			#print(pname,word1)
			keep_list.append(pname)

	return (keep_list,remove_list)


def keep_remove_list2wrapper(daysmax=0,package_dict={},verbosity=0):
	keep_list,remove_list =  keep_remove_list2fuz(daysmax,package_dict,verbosity)
	if 0 == len(keep_list) and 0 == len(remove_list):
		# Fuzzy version of processing did not complete as expected so fall back to simple.
		keep_list,remove_list =  keep_remove_list2(daysmax,package_dict,verbosity)
	return (keep_list,remove_list)


def deb_local(daysmax=0,glob_pattern='/var/cache/apt/archives/*.deb',verbosity=0):

	package_dict = {}
	try:
		glob_matches = glob(glob_pattern)
	except AttributeError:
		glob_matches = []
	for ppath in glob_matches:
		pname = path.basename(ppath)
		#pname0 = pname.split('.')[0]
		#pdir = path.dirname(pname)
		if pname is None:
			raise Exception("# Error during processing match {0}".format(ppath))
		nixtime_modified = int(stat(ppath).st_mtime)
		#print(nixtime_modified)
		package_dict[pname]=nixtime_modified
		if verbosity < 1:
			continue
		print("# {0} {1}".format(pname,nixtime_modified))
	# Sort the dictionary so ordered on nixtime
	return package_dict


if __name__ == '__main__':

	exit_rc = 0
	package_dict = deb_local()
	if len(package_dict) < 1:
		exit_rc = 154
	if len(package_dict) > 1:
		#keep_remove_verbosity=3
		keep_remove_verbosity=0
		keep_list,remove_list = keep_remove_list2wrapper(0,package_dict,
								verbosity=keep_remove_verbosity)

	#for pname in keep_list:
	#	print(pname)

	if len(remove_list) > 0:
		lines = write_rm(package_dict,remove_list,2)
	exit(exit_rc)

